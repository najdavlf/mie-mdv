import numpy as np
"""
data_n_wl=[0.2,0.225,0.25,0.275,0.3,0.325,0.35,0.375,0.4,0.425,0.45,0.475,0.5,0.525,0.55,0.575,0.6,0.625,0.65,0.675,0.7,0.725,0.75,0.775,0.8,0.825,0.85,0.875,0.9,0.925,0.95,0.975,1,1.2,1.4,1.6,1.8,2,2.2,2.4,2.6,2.65,2.7,2.75,2.8,2.85,2.9,2.95,3,3.05,3.1,3.15,3.2,3.25,3.3,3.35,3.4,3.45,3.5,3.6,3.7,3.8,3.9,4,4.1,4.2,4.3,4.4,4.5,4.6,4.7,4.8,4.9,5,5.1,5.2,5.3,5.4,5.5,5.6,5.7,5.8,5.9,6,6.1,6.2,6.3,6.4,6.5,6.6,6.7,6.8,6.9,7,7.1,7.2,7.3,7.4,7.5,7.6,7.7,7.8,7.9,8,8.2,8.4,8.6,8.8,9,9.2,9.4,9.6,9.8,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,21,22,23,24,25,26,27,28,29,30,32,34,36,38,40,42,44,46,48,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200]
data_n=[1.396,1.373,1.362,1.354,1.349,1.346,1.343,1.341,1.339,1.338,1.337,1.336,1.335,1.334,1.333,1.333,1.332,1.332,1.331,1.331,1.331,1.33,1.33,1.33,1.329,1.329,1.329,1.328,1.328,1.328,1.327,1.327,1.327,1.324,1.321,1.317,1.312,1.306,1.296,1.279,1.242,1.219,1.188,1.157,1.142,1.149,1.201,1.292,1.371,1.426,1.467,1.483,1.478,1.467,1.45,1.432,1.42,1.41,1.4,1.385,1.374,1.364,1.357,1.351,1.346,1.342,1.338,1.334,1.332,1.33,1.33,1.33,1.328,1.325,1.322,1.317,1.312,1.305,1.298,1.289,1.277,1.262,1.248,1.265,1.319,1.363,1.357,1.347,1.339,1.334,1.329,1.324,1.321,1.317,1.314,1.312,1.309,1.307,1.304,1.302,1.299,1.297,1.294,1.291,1.286,1.281,1.275,1.269,1.262,1.255,1.247,1.239,1.229,1.218,1.185,1.153,1.126,1.111,1.123,1.146,1.177,1.21,1.241,1.27,1.297,1.325,1.351,1.376,1.401,1.423,1.443,1.461,1.476,1.48,1.487,1.5,1.511,1.521,1.531,1.539,1.545,1.549,1.551,1.551,1.546,1.536,1.527,1.522,1.519,1.522,1.53,1.541,1.555,1.587,1.703,1.821,1.886,1.924,1.957,1.966,2.004,2.036,2.056,2.069,2.081,2.094,2.107,2.119,2.13]
def gsl_pow_2(l) :
    return l*l
def gsl_pow_4(l) :
    return l**4 
def gsl_pow_3(l) :
    return l**3
def gsl_pow_6(l) :
    return l**6
def sqrt(l) :
    return l**(.5)

def indiceEau(n) :
  # nu en metres
  # d'apres Thormahlen and al., Journal of Physical and Chemical Reference Data, 1985, 14, pp 933-945
  nu = float(n)
  return float(sqrt( 0.005743534/(gsl_pow_2(nu/1.E-6)-0.018085) +1.769238 -0.02797222*gsl_pow_2(nu/1.E-6) +0.008715348*gsl_pow_4(nu/1.E-6) -0.001413942*gsl_pow_6(nu/1.E-6)) + (-0.00008454823 -0.00002787742*gsl_pow_2(nu/1.E-6) +0.000002608176*gsl_pow_4(nu/1.E-6))*(36.-19.993) + (-0.000002050671 +0.000001019989*gsl_pow_2(nu/1.E-6) -0.000002611919*gsl_pow_4(nu/1.E-6))*gsl_pow_2(36.-19.993) + (0.000000008194989 -0.000000008107707*gsl_pow_2(nu/1.E-6) +0.00000004877274*gsl_pow_4(nu/1.E-6))*gsl_pow_3(36.-19.993))

dataw = [i for i in data_n_wl if i > 2.77]
datan = [data_n[i] for (i,j) in enumerate(data_n_wl) if j > 2.77]

dataw1 = np.linspace(.182,2.77,500,dtype="d")

f = open("myindiceRefraction","w")
f.write("# Longueur d'onde (m)\t n\t kappa\t n_ext\n")
for w in dataw1 :
  n=indiceEau(w*10.**(-6))
  f.write( "%10.14g\t%10.14g\t%g\t%g\n" %(w*10.**(-6),n,0.,1.))
for (w,n) in zip(dataw,datan) :
  f.write( "%10.14g\t%10.14g\t%g\t%g\n" %(w*10.**(-6),n,0.,1.))
f.close()
"""

### use only Quentin's complex index of refraction ###
f = open("refwater.dat","r")
fwrite = open("myIndiceRefraction","w")
fwrite.write("# Longueur d'onde (m)\tn\tkappa\tn_ext\n")
for l in f :
    v = l.split(" ")
    lamb = float(v[0])*1.e-6
    indr = float(v[1])
    indi = float(v[2])
    fwrite.write( "%10.14g\t%10.14g\t%g\t%g\n" %(lamb,indr,indi,1)) 
fwrite.close()
f.close()

lamb = np.arange(0.2,14.,.025)
fwrite = open("myListLambda","w")
for l in lamb :
    fwrite.write("%g\n" %(l))
fwrite.close()
