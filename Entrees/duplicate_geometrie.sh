base=geometrie-Ne_XX.txt 
for seff in 0.01 0.1 0.175 0.25
do
  new=geometrie-Ne_${seff}.txt
  cp $base $new
  sed -i "s/_Ne_/"$seff"/g" $new
done
