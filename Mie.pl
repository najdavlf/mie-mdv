#!/usr/bin/perl -w
use strict;
#use Math::Trig;
use PDL;
use PDL::Constants  qw(PI);
use PDL::GSL::CDF; # install : sudo cpan App::cpanminus puis sudo cpanm PDL::GSL::CDF

printf("\n\nCalcul des propriétés radiatives par Mie...\n");
###########################################################
#            SUPPRESSION DES ANCIENS RESULTATS            #
###########################################################
open(RM,"|rm -f Sorties/*.txt");
close(RM);
open(RM,"|rm -rf Sorties/*");
close(RM);
###########################################################
#                        CHEMINS                          #
###########################################################
#                        DONNEES                          #
my $nomFichiers = "Entrees/nomFichiers.txt";
#                      PROGRAMMES                         #
my $Mishchenko = "Programmes/Mishchenko/mishchenko";
#                       RESULTATS                         #
my $AnnexesFolder = "Sorties/Annexes";
open(MK,"|mkdir $AnnexesFolder");
close(MK);
###########################################################
#                CHARGEMENT DES PARAMETRES                #
###########################################################
#                     NOM FICHIERS                        #
my $lambdaFile;
my $indiceFile;
my $geometrieFile;
my $compoFile;
my $sectionsPartFile;
my $sectionsMassFile;
my $efficaciteFile;
my $gFile;
my $albedoFile;
my $pfFile;
my $cumuleeInvFile;
my $cumuleeInvReadmeFile;
my $cumuleeInvLambdaFile;
my $cumuleeInvCdfFile;
&loadFileNames();
#                   GEOMETRIE PARTICULE                   #
my $rMoyen;
my $largeurDistrib;
&loadGeometrie();
#                  COMPOSITION PARTICULE                  #
my $rhoSec;
&loadComposition();
#                  INDICE DE REFRACTION                   #
my @indiceLambda = ();
my @indiceRe = ();
my @indiceIm = ();
my @indiceMilieu = ();
my $j = 0;
&loadIndice();
my $indiceNb = $j;
###########################################################
#INITIALISATION DES FICHIERS DE SORTIE LAMBDA INDEPENDENTS#
###########################################################
#                 SECTIONS PARTICULAIRES                  #
open(FILE,">$sectionsPartFile") or die("open: $!");
printf(FILE "#Lambda (m)	section_efficace_absorption (m2/MO)	section_efficace_diffusion (m2/MO)	section_efficace_extinction (m2/MO)\n");
close(FILE);
#                   SECTIONS MASSIQUES                    #
open(FILE,">$sectionsMassFile") or die("open: $!");
printf(FILE "#Lambda (m)	section_efficace_absorption (m2/kgx)	section_efficace_diffusion (m2/kgx)	section_efficace_extinction (m2/kgx)\n");
close(FILE);
#                      EFFICACITES                        #
open(FILE,">$efficaciteFile") or die("open: $!");
printf(FILE "#Lambda (m)	efficacite_absorption	efficacite_diffusion	efficacite_extinction\n");
close(FILE);
#                       ASYMETRIE                         #
open(FILE,">$gFile") or die("open: $!");
printf(FILE "#Lambda (m)	g	b\n");
close(FILE);
#                        ALBEDO                           #
open(FILE,">$albedoFile") or die("open: $!");
printf(FILE "#Lambda (m)	albedo\n");
close(FILE);
#                   CUMULEE INV LAMBDA                    #
open(CP,"|cp $lambdaFile $cumuleeInvLambdaFile");
close(CP);
#                  CUMULEE INV SPECTRE                    #
open(FILE,">$cumuleeInvFile") or die("open: $!");
close(FILE);
###########################################################
#       CALCUL DE LA MASSE EFFECTIVE D UNE PARTICULE      #
###########################################################
my $Meff;
#my $r32;
my $Veff;
#$r32 = $rMoyen*1E-6*exp(5/2*log($largeurDistrib)**2);
$Veff = 4/3*PI*($rMoyen*1E-6)**3*exp(9/2*log($largeurDistrib)**2); # V moyen
$Meff = $Veff*$rhoSec; # M moyen
###########################################################
#CALCUL DES BORNES D'INTEGRATION DE LA DISTRIB DES TAILLES#
###########################################################
my $epsilon = 0.0002; # proportion de particules negligees
my $rMin = gsl_cdf_lognormal_Pinv($epsilon/2,log($rMoyen),log($largeurDistrib));
my $rMax = gsl_cdf_lognormal_Pinv(1-$epsilon/2,log($rMoyen),log($largeurDistrib));
#my $rTemp = 1.e-10;
#while (pdfLogNorm($rMoyen,$largeurDistrib,$rTemp)<1.e-4) {
#  $rTemp += 0.05;
#  }
#$rMin = $rTemp;
#$rTemp += 0.05;
#while (pdfLogNorm($rMoyen,$largeurDistrib,$rTemp)>1.e-4) {
#  $rTemp += 0.05;
#  }
#$rTemp += 0.05;
#$rMax = $rTemp;
###########################################################
#             CALCUL DES PROPRIETES RADIATIVES            #
###########################################################
my $Ca;
my $Cd;
my $Cext;
my $Qa;
my $Qd;
my $Qext;
my $g;
my $albedo;
my @cdfInvAngle = ();
my @cdfInvCdf = ();
my $cdfInvNbCdf;
#                  BOUCLE SUR LES LAMBDA                  #
my $NbLambda;
my $lambda;
my $indiceLambda;
$indiceLambda = 0;
open(LAMBDA,"<$lambdaFile") or die("open: $!");
while( <LAMBDA> ){
  chomp $_;
  if ($_ =~ /^#/) { next; }
  else {
    $lambda = $_;
    #                Avancement du calcul                     #
    printf("longueur d'onde %.14e m\n",$lambda);
    #            Ecriture du fichier mishchenko.in            #
    &writeMishchenkoInptut($lambda);
    #              Execution du code T-Matrice                #
    open(TMATRIX,"|./$Mishchenko");
    close(TMATRIX);
    #     Recuperation des resultats dans mishchenko.out      #
    &recupResults();
    #               Incrementation de l'indice                #
    $indiceLambda++;
    }
  }
close(LAMBDA);
# FIN BOUCLE
$NbLambda = $indiceLambda;
###########################################################
#             ECRITURE DE FICHIERS RESULTATS              #
###########################################################
#                    CUMULEE INV CDF                      #
open(FILE,">$cumuleeInvCdfFile") or die("open: $!");
for ($j = 0; $j < $cdfInvNbCdf; $j++) {
  printf(FILE "%.14e\n",$cdfInvCdf[$j]);
  }
close(FILE);
#                  CUMULEE INV README                     #
open(FILE,">$cumuleeInvReadmeFile") or die("open: $!");
printf(FILE "lignes: longueur d'onde
Nombre de longueurs d'onde: $NbLambda
colonnes: valeur cumulee R in [0,1]
Nombre de valeurs R: $cdfInvNbCdf
Les valeurs dans le fichier sont les angles de diffusion en radiant");
close(FILE);
###########################################################
#                   MESSAGE DE FIN                        #
###########################################################
printf("Calcul des propriétés radiative [OK]\n\n");
###########################################################
#                      FONCTIONS                          #
###########################################################
#               CHARGEMENT NOM FICHIERS                   #
sub loadFileNames {
	open(FILE,"<$nomFichiers") or die("open: $!");
	while( <FILE> ){
	  chomp $_;
	  if( $_ =~ m/[Ll]ongueurs d'onde \(en metres\)/ ) { 
	    $_ =~ s/.*: //;
	    $lambdaFile = "$_.txt"; }
	  if( $_ =~ m/[Ii]ndices complexes/ ) { 
	    $_ =~ s/.*: //;
	    $indiceFile = "$_.txt"; }
	  if( $_ =~ m/[Dd]istribution de taille/ ) { 
	    $_ =~ s/.*: //;
	    $geometrieFile = "$_.txt"; }
	  if( $_ =~ m/[Cc]omposition interne/ ) { 
	    $_ =~ s/.*: //;
	    $compoFile = "$_.txt"; }
	  if( $_ =~ m/[Ss]pectre sections \(m2\)/ ) { 
	    $_ =~ s/.*: //;
	    $sectionsPartFile = "$_.txt"; }
	  if( $_ =~ m/[Ss]pectre sections \(m2\/kgx\)/ ) { 
	    $_ =~ s/.*: //;
	    $sectionsMassFile = "$_.txt"; }
	  if( $_ =~ m/[Ss]pectre efficacites/ ) { 
	    $_ =~ s/.*: //;
	    $efficaciteFile = "$_.txt"; }
	  if( $_ =~ m/[Ss]pectre parametre asymetrie/ ) { 
	    $_ =~ s/.*: //;
	    $gFile = "$_.txt"; }
	  if( $_ =~ m/[Ss]pectre albedo/ ) { 
	    $_ =~ s/.*: //;
	    $albedoFile = "$_.txt"; }
	  if( $_ =~ m/[Ss]pectre cumulee inverse/ ) { 
	    $_ =~ s/.*: //;
	    $cumuleeInvFile = "$_.txt"; }
	  if( $_ =~ m/[Ff]onctions de phase/ ) { 
	    $_ =~ s/.*: //;
	    $pfFile = $_; }
	  if( $_ =~ m/Readme cumulee inverse/ ) { 
	    $_ =~ s/.*: //;
	    $cumuleeInvReadmeFile = "$_.txt"; }
	  if( $_ =~ m/Maillage lambda cumulee/ ) { 
	    $_ =~ s/.*: //;
	    $cumuleeInvLambdaFile = "$_.txt"; }
	  if( $_ =~ m/Maillage Cdf cumulee/ ) { 
	    $_ =~ s/.*: //;
	    $cumuleeInvCdfFile = "$_.txt"; }
	  }
	close( FILE );
	}
#          CHARGEMENT PARAMETRES GEOMETRIQUES             #
sub loadGeometrie {
	open(FILE,"<$geometrieFile") or die("open: $!");
	while( <FILE> ){
	  chomp $_;
	  if( $_ =~ m/Rayon moyen \(microns\)/ ) { 
	    $_ =~ s/.*: //;
	    $rMoyen = $_; }
	  if( $_ =~ m/Deviation \(sigma\)/ ) { 
	    $_ =~ s/.*: //;
	    $largeurDistrib = $_; }
	  }
	close( FILE );
	}
#               CHARGEMENT COMPOSITION                    #
sub loadComposition {
	open(FILE,"<$compoFile") or die("open: $!");
	while( <FILE> ){
	  chomp $_;
	  if( $_ =~ m/Densite de la particule/ ) { 
	    $_ =~ s/.*: //;
	    $rhoSec = $_; }
	  }
	close( FILE );
	}
#              CHARGEMENT SPECTRE INDICE                  #
sub loadIndice {
	open(FILE,"<$indiceFile") or die("open: $!");
	while( <FILE> ){
	  chomp $_;
	  if ($_ =~ /^#/) { next; }
	  else {
	    ($indiceLambda[$j],$indiceRe[$j],$indiceIm[$j],$indiceMilieu[$j]) = split(/\t/,$_);
	    #printf("\nData[%d]=%f",$j,$indiceRe[$j]);
	    $j++;
	    }
	  }
	close( FILE );
	}
#                INTERPOLATION LINEAIRE                   #
sub indiceRe {
	my $i = int(($_[0]-$indiceLambda[0])/($indiceLambda[$indiceNb-1]-$indiceLambda[0])*($indiceNb-1));
	my $val;
	if ( $i == $indiceNb-1 ) {$val = $indiceRe[$indiceNb-1];}
	else {$val = $indiceRe[$i] + ($indiceRe[$i+1]-$indiceRe[$i])/($indiceLambda[$i+1]-$indiceLambda[$i])*($_[0]-$indiceLambda[$i]);}
	return $val;
	}
sub indiceIm {
	my $i = int(($_[0]-$indiceLambda[0])/($indiceLambda[$indiceNb-1]-$indiceLambda[0])*($indiceNb-1));
	my $val;
	if ( $i == $indiceNb-1 ) {$val = $indiceIm[$indiceNb-1];}
	else {$val = $indiceIm[$i] + ($indiceIm[$i+1]-$indiceIm[$i])/($indiceLambda[$i+1]-$indiceLambda[$i])*($_[0]-$indiceLambda[$i]);}
	return $val;
	}
sub indiceMilieu {
	my $i = int(($_[0]-$indiceLambda[0])/($indiceLambda[$indiceNb-1]-$indiceLambda[0])*($indiceNb-1));
	my $val;
	if ( $i == $indiceNb-1 ) {$val = $indiceMilieu[$indiceNb-1];}
	else {$val = $indiceMilieu[$i] + ($indiceMilieu[$i+1]-$indiceMilieu[$i])/($indiceLambda[$i+1]-$indiceLambda[$i])*($_[0]-$indiceLambda[$i]);}
	return $val;
	}
#           Distribution de taille log-normale            #
#sub pdfLogNorm {
#my $rM = $_[0];
#my $s = $_[1];
#my $r = $_[2];
#return exp(-(log($r)-log($rM))**2./(2.*(log($s))**2.))/(sqrt(2.*PI)*$r*log($s));
#	}
#            Ecriture du fichier mishchenko.in            #
sub writeMishchenkoInptut {
	open(PARAM,">$Mishchenko.in") or die("open: $!");
	printf(PARAM "#Longueur d'onde
%.14e
#Partie reelle de l'indice de refraction
%.14e
#Partie imaginaire de l'indice de refraction
%.14e
#Rayon moyen de la distribution log-normal
%.14e
#Deviation de la distribution log-normal (sigma)
%.14e
#Borne inferieure de l'intervalle d'integration
%.14e
#Borne superieure de l'intervalle d'integration
%.14e
",$_[0]/(&indiceMilieu($_[0])*1.E-6),&indiceRe($_[0])/&indiceMilieu($_[0]),&indiceIm($_[0])/&indiceMilieu($_[0]),$rMoyen,$largeurDistrib,$rMin,$rMax);
	close( PARAM );
	}
#           RECUPERATION ET ECRITURE DES RESULTATS        #
sub recupResults {
open(PROG,"|sh Programmes/Mishchenko/make_data_mishchenko_out");
close(PROG);
open(FILE,"<$Mishchenko.out") or die("open: $!");
while( <FILE> ){
  chomp $_;
  if( $_ =~ m/C_extinction/ ) { 
    $_ =~ s/.*:\t//;
    $Cext = $_*1E-12; }
  if( $_ =~ m/C_diffusion/ ) { 
    $_ =~ s/.*:\t//;
    $Cd = $_*1E-12; }
  if( $_ =~ m/C_absorption/ ) { 
    $_ =~ s/.*:\t//;
    $Ca = $_*1E-12; }
  if( $_ =~ m/Q_extinction/ ) { 
    $_ =~ s/.*:\t//;
    $Qext = $_; }
  if( $_ =~ m/Q_diffusion/ ) { 
    $_ =~ s/.*:\t//;
    $Qd = $_; }
  if( $_ =~ m/Q_absorption/ ) { 
    $_ =~ s/.*:\t//;
    $Qa = $_; }
  if( $_ =~ m/Albedo/ ) { 
    $_ =~ s/.*:\t//;
    $albedo = $_; }
  if( $_ =~ m/Parametre d asymetrie/ ) { 
    $_ =~ s/.*:\t//;
    $g = $_; }
  }
close( FILE );
###########################################################
#            ECRTITURE DES FICHIERS RESULTATS             #
###########################################################
#                 SECTIONS PARTICULAIRES                  #
open(FILE,">>$sectionsPartFile") or die("open: $!");
printf(FILE "%.14e\t%.14e\t%.14e\t%.14e\n",$lambda,$Ca,$Cd,$Cext);
close(FILE);
#                   SECTIONS MASSIQUES                    #
open(FILE,">>$sectionsMassFile") or die("open: $!");
printf(FILE "%.14e\t%.14e\t%.14e\t%.14e\n",$lambda,$Ca/$Meff,$Cd/$Meff,$Cext/$Meff);
close(FILE);
#                      EFFICACITES                        #
open(FILE,">>$efficaciteFile") or die("open: $!");
printf(FILE "%.14e\t%.14e\t%.14e\t%.14e\n",$lambda,$Qa,$Qd,$Qext);
close(FILE);
#                        ALBEDO                           #
open(FILE,">>$albedoFile") or die("open: $!");
printf(FILE "%.14e\t%.14e\n",$lambda,$albedo);
close(FILE);
#                   FONCTION DE PHASE                     #
# chargement :
my @anglePF = ();
my @PF = ();
my $NbanglePF;
open(FILE,"<Programmes/Mishchenko/PF.txt") or die("open: $!");
$j = 0;
while( <FILE> ){
  chomp $_;
  ($anglePF[$j],$PF[$j]) = split(/\t/,$_);
  #printf("\nData[%d]=%.14e",$j,$PF[$j]);
  $j++;
  }
close( FILE );
$NbanglePF = $j;
# deplacement :
open(CP,"|mv Programmes/Mishchenko/PF.txt $pfFile\_${lambda}.txt");
close(CP);
#               SPECTRE CUMULEE INVERSE                   #
# construction de la cumulee
# et calcul de la FRACTION RETRO-DIFFUSEE :               #
my @cdf = ();
my $b = 0;
my $trapz;
$cdf[0] = 0;
for ($j = 0; $j < $NbanglePF-1 ; $j++){
  $trapz = 0.5*($PF[$j+1]*sin($anglePF[$j+1])+$PF[$j]*sin($anglePF[$j]))*($anglePF[$j+1]-$anglePF[$j]);
  $cdf[$j+1] = $cdf[$j] + $trapz;
  if ($anglePF[$j] >= PI/2.) {
    $b += $trapz;
    }
  }
# normalisation de la cumulee :
my $norme = $cdf[$NbanglePF-1];
for ($j = 0; $j < $NbanglePF; $j++){
  $cdf[$j] = $cdf[$j]/$norme;
  }
# normalisation de la FRACTION RETRO-DIFFUSEE             #
$b = $b/$norme;
#        ECRITURE PARAMETRE D ASYMETRIE                   #
open(FILE,">>$gFile") or die("open: $!");
printf(FILE "%.14e\t%.14e\t%.14e\n",$lambda,$g,$b);
close(FILE);
#               SPECTRE CUMULEE INVERSE                   #
# inversion et interpolation :
$cdfInvNbCdf = 2*$NbanglePF;
my $i = 0;
for ($j = 0; $j < $cdfInvNbCdf; $j++) {
  $cdfInvCdf[$j] = 1.*$j/($cdfInvNbCdf-1);
  while ($cdfInvCdf[$j] > $cdf[$i+1]) { $i++; }
  if ( $i == $NbanglePF-1 ) { $cdfInvAngle[$j] = $anglePF[$i] ; }
  else { $cdfInvAngle[$j] = $anglePF[$i] + ($anglePF[$i+1]-$anglePF[$i])/($cdf[$i+1]-$cdf[$i])*($cdfInvCdf[$j]-$cdf[$i]);}
  }
# ecriture :
open(FILE,">>$cumuleeInvFile") or die("open: $!");
for ($j = 0; $j < $cdfInvNbCdf; $j++) {
  printf(FILE "%.14e\t",$cdfInvAngle[$j]);
  }
printf(FILE "\n");
close(FILE);
#        ARCHIVAGE DES RESULTATS INTERMEDIAIRES           #
open(MV,"|mv $Mishchenko.in $AnnexesFolder/mishchenko\_${lambda}.in");
close(MV);
open(MV,"|mv $Mishchenko.out $AnnexesFolder/mishchenko\_${lambda}.out");
close(MV);
}
