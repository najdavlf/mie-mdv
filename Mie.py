# coding: utf8
import numpy as np
import os,sys
from size_distrib import *
from mieReadInputs import *

print("\n\nCalcul des propriétés radiatives par Mie...\n");
###########################################################
#            SUPPRESSION DES ANCIENS RESULTATS            #
###########################################################
os.system("rm -f Sorties/*.txt");
os.system("rm -rf Sorties/*");
###########################################################
#                        CHEMINS                          #
###########################################################
#                        DONNEES                          #
nomFichiers = "Entrees/nomFichiers.txt";
#                      PROGRAMMES                         #
Mishchenko = "Programmes/Mishchenko/mishchenko";
#                       RESULTATS                         #
AnnexesFolder = "Sorties/Annexes";
os.system("mkdir "+AnnexesFolder);

###########################################################
#                CHARGEMENT DES PARAMETRES                #
###########################################################

#                     NOM FICHIERS                        #
fileNames = loadFileNames(nomFichiers)
lambdaFile=            fileNames["lambdaFile"           ]  
indiceFile=            fileNames["indiceFile"           ]
geometrieFile=         fileNames["geometrieFile"        ]
compoFile=             fileNames["compoFile"            ]
sectionsPartFile=      fileNames["sectionsPartFile"     ]
sectionsMassFile=      fileNames["sectionsMassFile"     ]
efficaciteFile=        fileNames["efficaciteFile"       ]
gFile=                 fileNames["gFile"                ]
albedoFile=            fileNames["albedoFile"           ]
pfFile=                fileNames["pfFile"               ]
cumuleeInvFile=        fileNames["cumuleeInvFile"       ]
cumuleeInvReadmeFile=  fileNames["cumuleeInvReadmeFile" ]
cumuleeInvLambdaFile=  fileNames["cumuleeInvLambdaFile" ]
cumuleeInvCdfFile=     fileNames["cumuleeInvCdfFile"    ]

#                   GEOMETRIE PARTICULE                   #
geomParams = loadGeometrie(geometrieFile)
distShape = geomParams["distShape"]
rEffectif = geomParams["rEffectif"] # effective radius
vEffectif = geomParams["vEffectif"] # effective variance
print("Shape %i, reff %g, veff %g\n"%(distShape,rEffectif,vEffectif))

#                  COMPOSITION PARTICULE                  #
rhoSec = loadComposition(compoFile)*1e-18 # en kg / um3
#                  INDICE DE REFRACTION                   #
indicesRef = loadIndice(indiceFile);
indiceLambda =  indicesRef["indiceLambda"]
indiceRe     =  indicesRef["indiceRe"    ]
indiceIm     =  indicesRef["indiceIm"    ]
indiceMilieu =  indicesRef["indiceMilieu"]
indiceNb = len(indiceMilieu)

###########################################################
#                      FONCTIONS                          #
###########################################################

#                INTERPOLATION LINEAIRE                   #
def findiceRe(lamda) :
  i=0
  while (lamda>indiceLambda[i+1]) : i+=1
  if ( i == indiceNb-1 ) : val = indiceRe[indiceNb-1]
  else : val = indiceRe[i] + (indiceRe[i+1]-indiceRe[i])/(indiceLambda[i+1]-indiceLambda[i])*(lamda-indiceLambda[i])
  return val

def findiceIm(lamda) :
  i=0 ;
  while (lamda>indiceLambda[i+1]) : i+=1
  if ( i == indiceNb-1 ) : val = indiceIm[indiceNb-1]
  else : val = indiceIm[i] + (indiceIm[i+1]-indiceIm[i])/(indiceLambda[i+1]-indiceLambda[i])*(lamda-indiceLambda[i])
  return val

def findiceMilieu(lamda) :
  i=0 ;
  while (lamda>indiceLambda[i+1]) : i+=1 
  if ( i == indiceNb-1 ) : val = indiceMilieu[indiceNb-1]
  else : val = indiceMilieu[i] + (indiceMilieu[i+1]-indiceMilieu[i])/(indiceLambda[i+1]-indiceLambda[i])*(lamda-indiceLambda[i])
  return val

def BB(distShape,re,ve) :
  if distShape==2 :   # lognormale
    BB = np.log(ve+1.) # ve = exp(ln2sg)-1
  elif distShape==4 : # gamma
    BB = ve 
  else : 
    print("wrong distribution shape", distShape)
    BB = 0
  return BB

def AA(distShape,re,ve) :
  if distShape==2 :   # lognormale
    ln2sg = BB(2,re,ve)
    AA = re*np.exp(-5./2.*ln2sg)
  elif distShape==4 : # gamma
    AA = re 
  else : 
    print("wrong distribution shape",distShape)
    AA = 0
  return AA 

#            Ecriture du fichier mishchenko.in            #
def writeMishchenkoInput(lamda) :
  f=open(Mishchenko+".in","w")
  f.write("""#Longueur d'onde 
%.14e
#Partie reelle de l'indice de refraction
%.14e 
#Partie imaginaire de l'indice de refraction
%.14e
#Forme de la distribution (2:lognorm, 4:gamma)
%i
#Parametre AA de la distirbution  (2:geometric mean r, 4:a)
%.14e
#Parametre BB de la distribution (2:log(geometric std)^2, 4:b)
%.14e
#Borne inferieure de l'intervalle d'integration
%.14e
#Borne superieure de l'intervalle d'integration
%.14e"""\
  %(lamda/(findiceMilieu(lamda)*1.E-6),findiceRe(lamda)/findiceMilieu(lamda),findiceIm(lamda)/findiceMilieu(lamda),distShape,AA(distShape,rEffectif,vEffectif),BB(distShape,rEffectif,vEffectif),rMin,rMax));
  f.close();

#           RECUPERATION ET ECRITURE DES RESULTATS        #
def recupResults(strlamda) :
  os.system("sh Programmes/Mishchenko/make_data_mishchenko_out");
  f=open(Mishchenko+".out","r") 
  for line in f :
    if ':' in line :
      name=line.split(":")[-1].strip()
    else : continue 
    if "C_extinction" in line :
      Cext = np.float(name)*1E-12
    elif "C_diffusion" in line :
      Cd = np.float(name)*1E-12
    elif "C_absorption" in line :
      Ca = np.float(name)*1E-12
    elif "Q_extinction" in line :
      Qext =  np.float(name)
    elif "Q_diffusion" in line :
      Qd = np.float(name)
    elif "Q_absorption" in line :
      Qa = np.float(name)
    elif "Albedo" in line : 
      albedo = np.float(name)
    elif "Parametre d asymetrie" in line :
      g = np.float(name)
  f.close();
  ###########################################################
  #            ECRTITURE DES FICHIERS RESULTATS             #
  ###########################################################
  #                 SECTIONS PARTICULAIRES                  #
  f=open(sectionsPartFile,"a")
  f.write("%.14e\t%.14e\t%.14e\t%.14e\n"%(lamda,Ca,Cd,Cext))
  f.close();
  #                   SECTIONS MASSIQUES                    #
  f=open(sectionsMassFile,"a")
  f.write("%.14e\t%.14e\t%.14e\t%.14e\n"%(lamda,Ca/Meff,Cd/Meff,Cext/Meff))
  f.close();
  #                      EFFICACITES                        #
  f=open(efficaciteFile,"a")
  f.write("%.14e\t%.14e\t%.14e\t%.14e\n"%(lamda,Qa,Qd,Qext))
  f.close();
  #                        ALBEDO                           #
  f=open(albedoFile,"a")
  f.write("%.14e\t%.14e\n"%(lamda,albedo))
  f.close();
  #                   FONCTION DE PHASE                     #
  # chargement :
  anglePF = []
  PF = []
  f=open("Programmes/Mishchenko/PF.txt","r")
  for line in f :
    i1,i2 = line.split("\t")
    anglePF += [np.float(i1)]
    PF += [np.float(i2)]
  f.close();
  NbanglePF = len(PF)
  # deplacement :
  os.system("mv Programmes/Mishchenko/PF.txt "+pfFile+"_"+strlamda+".txt");
  #               SPECTRE CUMULEE INVERSE                   #
  # construction de la cumulee
  # et calcul de la FRACTION RETRO-DIFFUSEE :               #
  cdf = [] 
  b = 0
  cdf += [0]
  for j in range(0,NbanglePF-1) :
    trapz = 0.5*(PF[j+1]*np.sin(anglePF[j+1])+PF[j]*np.sin(anglePF[j]))*(anglePF[j+1]-anglePF[j]);
    cdf += [cdf[j] + trapz];
    if (anglePF[j] >= np.pi/2.) :
      b += trapz;
  # normalisation de la cumulee :
  norme = cdf[NbanglePF-1];
  for j in range(0,NbanglePF) :
    cdf[j] = cdf[j]/norme;
  # normalisation de la FRACTION RETRO-DIFFUSEE             #
  b = b/norme;
  #        ECRITURE PARAMETRE D ASYMETRIE                   #
  f=open(gFile,"a")
  f.write("%.14e\t%.14e\t%.14e\n"%(lamda,g,b))
  f.close();
  #               SPECTRE CUMULEE INVERSE                   #
  # inversion et interpolation :
  cdfInvNbCdf = 2*NbanglePF;
  cdfInvCdf=np.zeros(cdfInvNbCdf)
  cdfInvAngle=np.zeros(cdfInvNbCdf)
  i = 0;
  for j in range(0,cdfInvNbCdf) :
    cdfInvCdf[j] = 1.*j/(cdfInvNbCdf-1)
    while (cdfInvCdf[j] > cdf[i+1]) : i+=1
    if ( i == NbanglePF-1 ) : cdfInvAngle[j] = anglePF[i] 
    else : cdfInvAngle[j] = anglePF[i] + (anglePF[i+1]-anglePF[i])/(cdf[i+1]-cdf[i])*(cdfInvCdf[j]-cdf[i])
  # ecriture :
  f=open(cumuleeInvFile,"a")
  for j in range(0,cdfInvNbCdf) :
    f.write("%.14e\t"%cdfInvAngle[j]);
  f.write("\n");
  f.close();
  #        ARCHIVAGE DES RESULTATS INTERMEDIAIRES           #
  os.system("mv "+Mishchenko+".in "+AnnexesFolder+"/mishchenko_"+strlamda+".in");
  os.system("mv "+Mishchenko+".out "+AnnexesFolder+"/mishchenko_"+strlamda+".out");
  return cdfInvCdf,cdfInvNbCdf

### PROG PRINCIPAL ## 
  
###########################################################
#INITIALISATION DES FICHIERS DE SORTIE LAMBDA INDEPENDENTS#
###########################################################
#                 SECTIONS PARTICULAIRES                  #
f=open(sectionsPartFile,"w")
f.write( "#Lambda (m)	section_efficace_absorption (m2/MO)	section_efficace_diffusion (m2/MO)	section_efficace_extinction (m2/MO)\n");
f.close();
#                   SECTIONS MASSIQUES                    #
f=open(sectionsMassFile,"w")
f.write("#Lambda (m)	section_efficace_absorption (m2/kgx)	section_efficace_diffusion (m2/kgx)	section_efficace_extinction (m2/kgx)\n");
f.close();
#                      EFFICACITES                        #
f=open(efficaciteFile,"w")
f.write("#Lambda (m)	efficacite_absorption	efficacite_diffusion	efficacite_extinction\n");
f.close();
#                       ASYMETRIE                         #
f=open(gFile,"w")
f.write("#Lambda (m)	g	b\n");
f.close();
#                        ALBEDO                           #
f=open(albedoFile,"w")
f.write("#Lambda (m)	albedo\n");
f.close();
#                   CUMULEE INV LAMBDA                    #
os.system("cp "+lambdaFile+" "+cumuleeInvLambdaFile);
#                  CUMULEE INV SPECTRE                    #
f=open(cumuleeInvFile,"w")
f.close()

###########################################################
#               SET DISTRIBUTION SHAPE                    #
###########################################################
if distShape == 2 :   # lognormale 
    pdf=plognorm
elif distShape == 4 : # gamma
    pdf=pgamma
else : 
    print("wrong distribution shape",distShape)
###########################################################
#CALCUL DES BORNES D'INTEGRATION DE LA DISTRIB DES TAILLES#
###########################################################
print("\n\tCalcul des bornes d'intégration en taille...\n");
eps = 0.0001
rMin = find_rmin(pdf,rEffectif,vEffectif,eps)
rMax = find_rmax(pdf,rEffectif,vEffectif,eps)
print("\n\tOK rMin %g rMax %g\n"%(rMin,rMax));
###########################################################
#       CALCUL DE LA MASSE EFFECTIVE D UNE PARTICULE      #
###########################################################
# ATTENTION, rhoSec n'est pas utilisé ici, directement fourni
# dans size_distrib.py !!!
Meff = compute_Me(pdf,rMin,rMax,rEffectif,vEffectif)
###########################################################
#             CALCUL DES PROPRIETES RADIATIVES            #
###########################################################
#                  BOUCLE SUR LES LAMBDA                  #
iLambda = 0;
print("\n\tCalcul Mie f (lamda)...\n");
f=open(lambdaFile,"r")
for line in f :
  if line.strip().startswith("#") : continue
  else :
    strlamda=line.strip()
    lamda = np.float(strlamda)
    strlamda='{:08.3f}e-6'.format(lamda*1e6)
    #                Avancement du calcul                     #
    print("longueur d'onde %.14e m\n"%lamda);
    #            Ecriture du fichier mishchenko.in            #
    writeMishchenkoInput(lamda);
    os.system("cat "+Mishchenko+".in")
    #              Execution du code T-Matrice                #
    print("\n")
    if os.path.isfile(Mishchenko)  :
      os.system("cp "+Mishchenko+" .")
    else : print("FATAL: need to compile "+Mishchenko+".f") ; exit(1)
    print("Running mishchenko code")
    os.system("./mishchenko")
    #     Recuperation des resultats dans mishchenko.out      #
    print("\n")
    print("Reading results")
    cdfInvCdf,cdfInvNbCdf=recupResults(strlamda);
    #               Incrementation de l'indice                #
    iLambda+=1;
f.close();
# FIN BOUCLE
NbLambda = iLambda;
print("\n\tEcriture des resultats...\n");
###########################################################
#             ECRITURE DE FICHIERS RESULTATS              #
###########################################################
#                    CUMULEE INV CDF                      #
f=open(cumuleeInvCdfFile,"w")
for j in range(0,cdfInvNbCdf) :
  f.write("%.14e\n"%cdfInvCdf[j]);
f.close();
#                  CUMULEE INV README                     #
f=open(cumuleeInvReadmeFile,"w")
f.write("""lignes: longueur d'onde
Nombre de longueurs d'onde:"""+str(NbLambda)+"""
colonnes: valeur cumulee R in [0,1]
Nombre de valeurs R:"""+str(cdfInvNbCdf)+"""
Les valeurs dans le fichier sont les angles de diffusion en radians""");
f.close();
###########################################################
#                   MESSAGE DE FIN                        #
###########################################################
print("Calcul des propriétés radiative [OK]\n\n");
