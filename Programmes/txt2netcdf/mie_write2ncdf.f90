      program write2ncdf

        use netcdf

        implicit none 
        double precision, parameter ::pi=3.141592654
        character(len = *),parameter::LAMB_FILE="Entrees/listeLambdaDefault.txt"
        !"
        character(len = *),parameter::DIST_FILE="Entrees/geometrie.txt"
        character(len = *),parameter::G_FILE="Sorties/asymetrie.txt"
        character(len = *),parameter::SSA_FILE="Sorties/albedo.txt"
        character(len = *),parameter::SM_FILE="Sorties/sectionsMass.txt"
        character(len = *),parameter::SP_FILE="Sorties/sectionsPart.txt"
        character(len = *),parameter::EF_FILE="Sorties/efficacite.txt"
        character(len = *),parameter::PDF_FILE="Sorties/ALL_PF.txt"
        character(len = *),parameter::iCDF_FILE="Sorties/CumuleeInv.txt"
        character(len = *),parameter::CDF_FILE="Sorties/CumulInvCdf.txt"
        character(len = *),parameter::FILE_NAME="Mie_LUT_Cloud.nc"
        !"
        character(len = 9) :: dist_type 
        integer ios,i,j,k
        integer, parameter :: NDIMS=3
        integer :: NNU, NCP, NTH, NDD=1
        integer :: lambda_dimid, th_dimid, mu_dimid, cp_dimid, di_dimid
        integer :: dimidsth(NDIMS), dimidscp(NDIMS), dimidslambda(NDIMS-1)
        integer :: ncid, lambdavarid, thvarid, muvarid, cpvarid, distvarid
        integer :: ecsvarid, scsvarid, acsvarid, gvarid, ssavarid
        integer :: mecsvarid,mscsvarid,macsvarid
        integer :: eqfvarid, sqfvarid, aqfvarid
        integer :: phasevarid, pdfvarid, cpdfvarid, icpvarid, muicpvarid
        integer :: veffvarid, reffvarid
        integer :: Ndistr
        double precision, allocatable, dimension(:) :: lambda, th, mu, cp
        double precision, allocatable, dimension(:,:) :: ssa, g
        double precision, allocatable, dimension(:,:) :: ecs, scs, acs
        double precision, allocatable, dimension(:,:) :: mecs, mscs, macs
        double precision, allocatable, dimension(:,:) :: eqf, sqf, aqf
        double precision, allocatable, dimension(:,:,:) :: phase, pdf, cpdf, icp, mu_icp
        double precision :: tmp,y,yj,yn,x,xj,xn,dy
        double precision :: reff, veff


!      ###### Read nb of wavelengths #####

        open(11, file=LAMB_FILE, status="old", &
                iostat=ios)
        if (ios.ne.0) then
                write(*,*) "Data file could not be opened"
        else
         i=-1
         do while(ios.eq.0)
          i=i+1
          read(11,*,iostat=ios) 
         enddo
        endif
        close(11)
        NNU=i

!       #### Read distrib ####

        open(11, file=DIST_FILE, status="old", &
                iostat=ios) 
        if (ios.ne.0) then
                write(*,*) "Cloud geometry file could not be opened"
        else 
          do i=1,3
            read(11,*)
          enddo
          read(11,*) dist_type,dist_type,dist_type,          Ndistr
          read(11,*) dist_type,dist_type,dist_type,dist_type,reff
          read(11,*) dist_type,dist_type,dist_type,dist_type,veff
        endif
        close(11)

        if (Ndistr .eq. 2 ) then
          dist_type="L" ! stands for Lognormal
        else if (Ndistr .eq. 4) then
          dist_type="G" ! stands for Gamma
        else 
          dist_type="U" ! stands for Unknown
        endif

!       #### Read nb of angles ####

        open(11, file=PDF_FILE, status="old", &
                iostat=ios)
        if (ios.ne.0) then
                write(*,*) "PDF file could not be opened"
        else
          i=-1
          do while (ios .eq. 0)
            read(11,*, iostat=ios)
            i=i+1
          enddo
        endif
        close(11)
        if (i.gt.0) then
                NTH = dble(i)/dble(NNU)
        else 
          write(*,*) "Error while reading phase functions in ALL_PF file"
          !"
        endif

        open(11, file=CDF_FILE, status="old", &
                 iostat=ios)
        if (ios.ne.0) then
                write(*,*) "CDF file could not be opened"
        else
          i=-1
          do while (ios .eq. 0)
            read(11,*, iostat=ios)
            i=i+1
          enddo
        endif
        close(11)
        if (i.gt.0) then
             NCP = i
        else 
          write(*,*) "Error while reading phase functions in CDF file"
          !"
        endif


!      ###### Allocate arrays #####

        allocate(lambda(NNU), th(NTH), mu(NTH), cp(NCP))
        allocate(ecs(NNU,1), scs(NNU,1), acs(NNU,1), g(NNU,1), ssa(NNU,1))
        allocate(mecs(NNU,1), mscs(NNU,1), macs(NNU,1))
        allocate(eqf(NNU,1), sqf(NNU,1), aqf(NNU,1))
        allocate(phase(NTH,NNU,1), pdf(NTH,NNU,1), cpdf(NTH,NNU,1), icp(NCP,NNU,1), mu_icp(NCP,NNU,1))
 
!      ###### Read results into arrays #####

        open(11, file=CDF_FILE, status="old", &
                iostat=ios)
        if (ios.ne.0) then
                write(*,*) "CDF file could not be opened"
        else
          do i=1,NCP
            read(11,*) cp(i)
          enddo
        endif
        close(11)

        open(17, file=SSA_FILE, status="old")
        read(17,*)
        open(11, file=G_FILE, status="old") 
        read(11,*)
        open(12, file=SM_FILE, status="old")
        read(12,*)
        open(13, file=SP_FILE, status="old")
        read(13,*)
        open(14, file=EF_FILE, status="old")
        read(14,*)
        open(15, file=PDF_FILE, status="old")
        open(16, file=iCDF_FILE, status="old")

        do i=1,NNU
            read(11,*) lambda(i), g(i,1), tmp ! m
            read(17,*) tmp, ssa(i,1)
            read(12,*) tmp, macs(i,1), mscs(i,1), mecs(i,1)    ! m^2/kg
            read(13,*) tmp, acs(i,1) , scs(i,1) , ecs(i,1)     ! m^2/part
            read(14,*) tmp, aqf(i,1),  sqf(i,1) , eqf(i,1)   
            do j=1,NTH
              read(15,*) tmp, phase(j,i,1)
              if (i.eq.1) then
                th(j) = tmp
                mu(j) = dcos(tmp)
              endif
            enddo
            read(16,*) icp(1:NCP,i,1)
        enddo
        close(11)
        close(12)
        close(13)
        close(14)
        close(15)
        close(16)
        close(17)

        mu_icp = dcos(icp)

!       # Retrieve cumulative and normalize pdf #
        
        do i=1,NNU
         cpdf(j,1,1) = 0.
         do j=2,NTH ! iterative integration of pdf(mu)*dmu from 1 to -1
           cpdf(j,i,1) = cpdf(j-1,i,1) + (phase(j,i,1)+phase(j-1,i,1))/2. &
                                    *(mu(j-1)-mu(j))
         enddo
         pdf(:,i,1) = phase(:,i,1)/cpdf(NTH,i,1)
         cpdf(:,i,1)= cpdf(:,i,1)/cpdf(NTH,i,1)
        enddo

!       ###### NetCDF ######

!       Write results in netcdf file
        ! Create file
        call check( nf90_create(FILE_NAME, NF90_CLOBBER, ncid) )

        ! Define dimensions
        call check( nf90_def_dim(ncid, "lambda", NNU, lambda_dimid) )
        call check( nf90_def_dim(ncid, "th", NTH, th_dimid) )
        call check( nf90_def_dim(ncid, "mu", NTH, mu_dimid) )
        call check( nf90_def_dim(ncid, "cp", NCP, cp_dimid) )
        call check( nf90_def_dim(ncid, "distribution", NF90_UNLIMITED, di_dimid) )
        dimidsth = (/ th_dimid , lambda_dimid, di_dimid /) ! pdf, cpdf, phase
        dimidscp = (/ cp_dimid , lambda_dimid, di_dimid /)
        dimidslambda = (/ lambda_dimid, di_dimid /)

        ! Define variables
        ! wavelengths (nanometers)
        write(*,*) "lambda"
        call nc_def_var(ncid, (/lambda_dimid/), "lambda"     , &
                        "Wavelength","nanometers", &
                        lambdavarid)
        ! angles where phase function was computed
        write(*,*) "th"
        call nc_def_var(ncid, (/th_dimid/), "th", &
                        "theta","radian"    , & 
                        thvarid)
        ! cosine of angle where phase function was computed
        call nc_def_var(ncid, (/mu_dimid/), "mu",     &
                        "mu=cos(theta)","radian", & 
                        muvarid)
        ! regular cumulative of normalized phase function
        call nc_def_var(ncid, (/cp_dimid/), "cp", &
                        "cdf(mu)","-"       , & 
                        cpvarid)
        write(*,*) "Cross sec"
        ! extinction cross section as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "ecs",            &
                        "Extinction cross section","m^2", &
                        ecsvarid)
        ! scattering cross section as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "scs", &
                        "Scattering cross section","m^2", &
                        scsvarid)
        ! absorption cross section as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "acs", &
                        "Absorption cross section","m^2", &
                        acsvarid)
        ! massic extinction cross section as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "mecs", &
                        "Massic extinction cross section", "m^2/kg", &
                        mecsvarid)
        ! massic scattering cross section as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "mscs", &
                        "Massic scattering cross section", "m^2/kg", &
                        mscsvarid)
        ! massic absorption cross section as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "macs", &
                        "Massic absorption cross section", "m^2/kg", &
                        macsvarid)
        write(*,*) "Qual fac"
        ! extinction quality factor as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "eqf", &
                        "Extinction quality factor", "-", &
                        eqfvarid)
        ! scattering quality factor as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "sqf", &
                        "Scattering quality factor", "-", &
                        sqfvarid)
        ! absorption quality factor as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "aqf", &
                        "Absorption quality factor", "-", &
                        aqfvarid)
        ! assymetry parameter      as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "g", &
                        "Asymmetry parameter","-", &
                        gvarid)
        ! single scattering albedo as a function of wavelength
        call nc_def_var(ncid, dimidslambda, "ssa", &
                        "Single scattering albedo", "-", &
                        ssavarid)
        write(*,*) "Phase funcs"
        ! phase function as a function of angles and wavelength
        call nc_def_var(ncid, dimidsth, "phase_func", &
        "Mie scattering phase function as a function of theta",&
        "m^-1.sr^-1",   phasevarid)
        ! density funtion (normalized phase func) as a function of angles and wavelength
        call nc_def_var(ncid, dimidsth, "pdf",&
        "Mie scattering angle density probability as a function of theta", &
        !"
        "-", pdfvarid)
        ! cumulative phase function as a function of angles and wavelength
        call nc_def_var(ncid, dimidsth, "cpdf",&
        "Cumulative Mie scattering angle density probability", "-", &
                        cpdfvarid)
        ! inverse cumulative phase function (scattering angle at given cp and wavelength)
        call nc_def_var(ncid, dimidscp, "icp", &
        "Inverse of cumulative Mie scattering angle density probability",&
        "radian", icpvarid)
        ! cosine of inverse cumulative phase function (cos of scattering angle at given cp and wavelength)
        call nc_def_var(ncid, dimidscp, "mu_icp", &
        "Cosine of inverse of cumulative Mie scattering angle density probability",&
        !"
        "-", muicpvarid)
        write(*,*) "Distribs"
        ! Name of the distribution
        call nc_def_var_string(ncid, (/di_dimid/), "name", &
        "Name or identifier for the droplet size distribution",&
        "-", distvarid)
        ! effective radius of droplet size distribution
        call nc_def_var(ncid, (/di_dimid/), "reff", &
        "Effective radius of droplet size distribution",&
        "kilometers", reffvarid)
        ! effective variance of droplet size distribution
        call nc_def_var(ncid, (/di_dimid/), "veff", &
        "Effective variance of droplet size distribution",& 
        "micrometers", veffvarid)

        !" end definition
        call check( nf90_enddef(ncid) )

        ! set fields

        write(*,*) "lambda,th,mu,cp"
        call check( nf90_put_var(ncid, lambdavarid, lambda*1.E+9) )
        call check( nf90_put_var(ncid, thvarid, th) )
        call check( nf90_put_var(ncid, muvarid, mu) )
        call check( nf90_put_var(ncid, cpvarid, cp) )
        write(*,*) "Cross secs"
        call check( nf90_put_var(ncid, ecsvarid, ecs) )
        call check( nf90_put_var(ncid, scsvarid, scs) )
        call check( nf90_put_var(ncid, acsvarid, acs) )
        call check( nf90_put_var(ncid, mecsvarid, mecs) )
        call check( nf90_put_var(ncid, mscsvarid, mscs) )
        call check( nf90_put_var(ncid, macsvarid, macs) )
        call check( nf90_put_var(ncid, eqfvarid, eqf) )
        call check( nf90_put_var(ncid, sqfvarid, sqf) )
        call check( nf90_put_var(ncid, aqfvarid, aqf) )
        call check( nf90_put_var(ncid, ssavarid, ssa) )
        call check( nf90_put_var(ncid, gvarid, g) )
        write(*,*) "Phase funcs"
        call check( nf90_put_var(ncid, phasevarid, phase) )
        call check( nf90_put_var(ncid, pdfvarid, pdf) )
        call check( nf90_put_var(ncid, cpdfvarid, cpdf) )
        call check( nf90_put_var(ncid, icpvarid, icp) )
        call check( nf90_put_var(ncid, muicpvarid, mu_icp) )
        write(*,*) "Distributions"
        write(*,*) "Name"
        call check( nf90_put_var(ncid, distvarid, trim(dist_type)) )
        write(*,*) "reff"
        call check( nf90_put_var(ncid, reffvarid, reff*1.E-9) )
        write(*,*) "veff"
        call check( nf90_put_var(ncid, veffvarid, veff) )


        ! close file
        call check( nf90_close(ncid) )

        deallocate(lambda, th, mu, cp)
        deallocate(ecs, scs, acs, g, ssa)
        deallocate(mecs, mscs, macs)
        deallocate(eqf, sqf, aqf)
        deallocate(phase, pdf, cpdf, icp, mu_icp)
        
        contains
         subroutine check(status)
          integer, intent ( in) :: status
    
          if(status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped"
          end if
         end subroutine check

         subroutine nc_def_var(ncid,dimid,name,longname,units,varid)
           integer, intent(in) :: ncid
           integer, dimension(:), intent(in) :: dimid
           character(len=*), intent(in) :: name, longname, units 
           integer, intent(out) :: varid


           call check( nf90_def_var(ncid, name, NF90_DOUBLE, dimid, varid) )
           call check(nf90_put_att(ncid,varid,"long_name",longname) )
           call check(nf90_put_att(ncid,varid,"units",units) )

         end subroutine
        
         subroutine nc_def_var_string(ncid,dimid,name,longname,units,varid)
           integer, intent(in) :: ncid
           integer, dimension(:), intent(in) :: dimid
           character(len=*), intent(in) :: name, longname, units 
           integer, intent(out) :: varid


           call check( nf90_def_var(ncid, name, NF90_CHAR, dimid, varid) )
           call check(nf90_put_att(ncid,varid,"long_name",longname) )
           call check(nf90_put_att(ncid,varid,"units",units) )

         end subroutine

        end program
