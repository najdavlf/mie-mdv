# mie-MDV 
27/05/2020

## Overview 
- Outils pour réaliser des calculs de Mie, basés sur un code fortran de Mishchenko, 
configuré, exécuté, post-traité par 
  -- à l'origne, un script perl écrit par Jérémi Dauchet (Mie.pl)
  -- à présent, un script python traduit et modifié par Najda Villefranque (Mie.py)  
 (modif principale : choix de la forme de la distrib de taille dans Entrees/geometrie.txt) 

- Sorties des scripts python et Perl archivées dans des fichiers textes  
- Convertis au format netCDF par le script fortran mie_write2ncdf

01/2022 : compatible python3

Voir le fichier pdf dans doc/ pour plus d'information.

## Prérequis
- créer un répertoire Sorties 
  mkdir Sorties 
- compiler mishchenko.f avant première utilisation 
  cd Programmes/Mishchenko/ && bash make_mishchenko
- si besoin, compiler mie_write2ncdf.f90 avant première utilisation
  cd Programmes/txt2netcdf/ && make 
  peut avoir des pbs avec netCDF... 

## Utilisation

$ python Mie.py

pour une chaine de calcul avec conversion en netCDF, voir 
run_multiple_mie.sh

## Licenses

Tout le contenu de ce projet sauf mishchenko.f est sous licence GPLv3
Voir l'en-tête de mishchenko.f pour ce cas particulier 

Copyright 2002-2022 
 - Michael Mishchenko (mishchenko.f), 
 - Jérémi Dauchet (Mie.pl, Entrees/*[.txt,.sh]*) 
 - Najda Villefranque (*.[py,sh,md]*, mie_write2ncdf.f) 
