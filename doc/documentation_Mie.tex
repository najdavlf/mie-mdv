\documentclass[]{article}

\usepackage[margin=50pt]{geometry}
\usepackage{hyperref}

\newcommand{\reff}{r_{\mathrm{eff}}}
\newcommand{\veff}{v_{\mathrm{eff}}}
\newcommand{\rmin}{{r_{\min}}}
\newcommand{\rmax}{{r_{\max}}}

\title{Documentation pour utilisation du code de Mie}

\begin{document}
\maketitle

\section{Généralités}
Ce dépôt contient les outils nécessaires pour générer une table de propriétés optiques tabulées en longueur d'onde et en angle (pour les fonctions de phase uniquement), propriétés optiques issues de calculs de Mie. Le code Fortran de Mishchenko dans Programmes/Mishchenko permet de réaliser un calcul à une longueur d'onde donnée, en intégrant les propriétés optiques sur une distribution de taille donnée. La documentation complète de ce code \cite{mishchenko_scattering_2002} est disponible ici \url{https://www.giss.nasa.gov/staff/mmishchenko/publications/book_2.pdf}. Mie.py est une routine qui permet de préparer l'exécution du code de Mishchenko. C'est une adaptation d'une routine Perl Mie.pl écrite par Jérémi Dauchet. Je l'ai traduite en python car je n'arrivais pas à installer des modules Perl sans les droits admin alors que j'avais tout ce dont j'avais besoin en python. La tabulation spectrale est tous les 25 nm entre 200 nm et 13.975 $\mu$m puis aux points 14.286, 15.873, 20.000, 28.571 et 1000.000 $\mu$m. La liste des longueurs d'onde où le calul doit être fait est passé par le fichier listeLambdaDefault.txt. Il faut fournir les indices de réfraction de l'eau dans le fichier indiceRefractionDefault.txt. Ces indices de réfraction seront interpolés linéairement pour déterminer leur valeur aux longueurs d'ondes fournies dans listeLambdaDefault.txt.

\section{Distributions de taille}
La plus grosse difficulté est sur les données d'entrée, en particulier le choix de la distribution de taille. Dans le code de Mishchenko, on a 6 possibilités, réglables avec le mot clé NDISTR :
\begin{verbatim}
C     NDISTR specifies the type of particle size distribution 
C     as follows:        
C
C     NDISTR = 1 - modified gamma distribution                         
C          [Eq. (5.242) of Ref. 1]                                        
C              AA=alpha                                                
C              BB=r_c                                                  
C              GAM=gamma                                               
C     NDISTR = 2 - log normal distribution                             
C          [Eq. (5.243) of Ref. 1]                                        
C              AA=r_g                                                  
C              BB=[ln(sigma_g)]**2                                      
C     NDISTR = 3 - power law distribution                              
C          [Eq. (5.244) of Ref. 1]                                        
C               AA=r_eff (effective radius)                            
C               BB=v_eff (effective variance)                          
C               Parameters R1 and R2 (see below) are calculated        
C               automatically for given AA and BB                      
C     NDISTR = 4 - gamma distribution                                  
C          [Eq. (5.245) of Ref. 1]                                        
C               AA=a                                                   
C               BB=b                                                   
C     NDISTR = 5 - modified power law distribution                     
C          [Eq. (5.246) of Ref. 1]                                        
C               BB=alpha                                               
C     NDISTR = 6 - bimodal volume log normal distribution              
C              [Eq. (5.247) of Ref. 1]             
C              AA1=r_g1                                                
C              BB1=[ln(sigma_g1)]**2                                   
C              AA2=r_g2                                                
C              BB2=[ln(sigma_g2)]**2                                   
C              GAM=gamma                                               
C
\end{verbatim}
où Ref. 1 fait référence à \cite{mishchenko_scattering_2002}.
Le choix de la distribution de taille et des ses paramètres passe par les fichiers de configuration du script Mie.py. Ce script python lit des fichiers de configuration qui doivent être dans le dossier Entrees. Le fichier geometrie.txt donne les informations de la distribution de taille :
\begin{verbatim}
FORME : Sphere (Mie) = 1 ; Spheroide (Schiff) = 2 ; Cylindre (Schiff) = 3
Forme du diffuseur : 1
Facteur de forme (R) : 0.263
Distribution shape : _ND_
Rayon effectif (microns) : _Re_
Variance effective (microns) : _Ne_
\end{verbatim}

Les trois premières lignes ne sont pas utilisées par les scripts mais je les
laisse pour l'instant. Les trois lignes effectives sont Distribution shape,
Rayon effectif et Variance effective. Distribution shape correspond au numéro
donné dans la documentation de Mishchenko. Pour l'instant, seuls deux choix
sont prévus par Mie.py : 2 pour lognormale, 4 pour gamma. Les paramètres de
rayon effectif et de variance effective ne correspondent pas aux paramètres
attendus par le code de Mishchenko. Ces paramètres sont calculés en fonction du
rayon effectif et de la variance effective passées par geometrie.txt. Ceci
permet de comparer deux distributions de taille paramétrées "effectivement" de
la façon identique, mais qui prennent des paramètres différents. Soit $n(r)$ la
densité de gouttes dont le rayon est dans $\mathrm{d}r$, un intervalle
infinitésimal autour de $r$. Le rayon effectif est défini comme le rapport des
moments d'ordre 3 et 2 de la distribution de taille de gouttes, soit : 

\begin{equation}
\reff = \frac{\int_0^\infty \mathrm{d}r\, n(r) r^3}{\int_0^\infty \mathrm{d}r\, n(r) r^2}
\end{equation}

La variance effective représente un écart quadratique moyen au rayon effectif 
\begin{equation}
\veff = \frac{1}{\reff^2} \frac{\int_0^\infty \mathrm{d}r\, n(r)(r-\reff)^2 r^2}{\int_0^\infty \mathrm{d}r\, n(r) r^2}
\end{equation}

Ce sont ces paramètres qui sont fournis en entrée du script Mie.py. Les distributions de taille lognormale et gamma ainsi que leurs paramètres et caractéristiques sont données ci-dessous.

\subsection{Distribution log-normale}

\begin{equation}
n(r) = \frac{N_0}{\sqrt{2\pi}\ln\sigma_g} \frac{1}{r}\exp(-\frac{\ln{r}-\ln{r_g}}{2\ln^2{\sigma_g}})
\end{equation}
où $r_g$ et $s_g$ sont les moyennes et écarts types géométriques de la distribution. $r_g$ correspond également au rayon modal de la distribution. Le rayon moyen (pas utilisé ici) est donné par :
\begin{equation} 
\overline{r} = r_g \exp(\frac{1}{2}\ln^2\sigma_g)
\end{equation}
On peut montrer que 
\begin{equation}
\reff = r_g \exp(\frac{5}{2}\ln^2\sigma_g)
\end{equation}
et que 
\begin{equation}
\veff = \exp(\ln^2\sigma_g)-1
\end{equation}
Les paramètres attendus par le code de Mishchenko sont $AA=r_g$ et $BB=\ln^2\sigma_g$. Pour retrouver ces paramètres à partir de $\reff$ et $\veff$, on a :
\begin{equation}
AA = \reff\exp(-\frac{5}{2}\ln(\veff+1))
\end{equation}
\begin{equation}
BB = \ln(\veff+1)
\end{equation}

\subsection{Distribution gamma}

\begin{equation}
n(r) = N_0 r^{\frac{1-3b}{b}}\exp(-\frac{r}{ab})
\end{equation}
On peut montrer que 
\begin{equation}
\reff = a 
\end{equation}
et que 
\begin{equation}
\veff = b
\end{equation}

Les paramètres attendus par le code de Mishchenko sont $AA=a$ et $BB=b$, soit directement les paramètres entrés dans geometrie.txt. Le rayon modal de la distribution (non utilisé ici) est :
\begin{equation}
r_m = (1-3\veff)\reff
\end{equation}
et le paramètre de forme 
\begin{equation}
\alpha = \frac{1-3\veff}{\veff}
\end{equation}
Ces paramètres permettent de faire le lien avec la distribution gamma généralisée.

\subsection{Intervalle d'intégration}
Le code de Mishchenko intègre numériquement les propriétés optiques monodisperses sur la distribution de taille donnée. Pour ce faire, l'intervalle d'intégration $[\rmin,\rmax]$ est découpé en un nombre fixé de sous-intervalles. Pour déterminer $\rmin$ et $\rmax$, les fonctions find$\_$rmin et find$\_$rmax de size$\_$distrib.py sont utilisées. Elles prennent en paramètre, outre la distribution et ses paramètres, un $\epsilon$ qui est tel que :
\begin{equation}
\int_0^\rmin\mathrm{d}r n(r) \leq \epsilon
\end{equation}
et 
\begin{equation}
\int_\rmax^{1000}\mathrm{d}r n(r) \leq \epsilon
\end{equation}
C'est à dire que l'intégrale de la distribution de taille sur l'intervalle $[\rmin,\rmax]$ vaut $1-2\epsilon$. Dans Mie.py, $\epsilon=10^{-4}$.

\section{Calculs supplémentaires dans Mie.py}
Afin de convertir les sections efficaces [m$^{-1}$] en sections efficaces massiques [m$^2$.kg$^{-1}$], la masse effective est calculée à l'aide de la fonction compute$\_$Me de size$\_$distrib.py. La masse effective est définie comme 
\begin{equation}
\int_\rmin^\rmax \mathrm{d}r n(r)\, \rho_{w}\frac{4}{3} \pi r^3 
\end{equation}

Les fonctions de phase sont cumulées dans Mie.py par intégration numérique selon l'approximation des trapèzes appliquée à la fonction de phase et au sinus de l'angle de diffusion. La cumulée inverse est également tabulée, par interpolation linéaire de la cumulée. 

\section{Sorties}
Les sorties de Mie.py sont écrites au format ascii dans des fichiers archivés dans le dossier Sorties. Le script mie$\_$write2ncdf.f dans Programmes/txt2netcdf permet de convertir ces fichiers ascii au format netcdf. Le fichier netCDF en sortie contient les dimensions et variables suivantes : 

\begin{verbatim}
netcdf Mie_LUT_Cloud-2-7-0.1 {
dimensions:
  	lambda = 557 ;
  	th = 2000 ;
  	mu = 2000 ;
  	cp = 4000 ;
  	distribution = UNLIMITED ; // (1 currently)
variables:
  	double lambda(lambda) ;
    		lambda:long_name = "Wavelength" ;
    		lambda:units = "nanometers" ;
  	double th(th) ;
    		th:long_name = "theta" ;
    		th:units = "radian" ;
  	double mu(mu) ;
  		  mu:long_name = "mu=cos(theta)" ;
  		  mu:units = "radian" ;
  	double cp(cp) ;
  		  cp:long_name = "cdf(mu)" ;
  		  cp:units = "-" ;
  	double ecs(distribution, lambda) ;
  		  ecs:long_name = "Extinction cross section" ;
  		  ecs:units = "m^2" ;
  	double scs(distribution, lambda) ;
  		  scs:long_name = "Scattering cross section" ;
  		  scs:units = "m^2" ;
  	double acs(distribution, lambda) ;
  		  acs:long_name = "Absorption cross section" ;
  		  acs:units = "m^2" ;
  	double mecs(distribution, lambda) ;
  		  mecs:long_name = "Massic extinction cross section" ;
  		  mecs:units = "m^2/kg" ;
  	double mscs(distribution, lambda) ;
  		  mscs:long_name = "Massic scattering cross section" ;
  		  mscs:units = "m^2/kg" ;
  	double macs(distribution, lambda) ;
  		  macs:long_name = "Massic absorption cross section" ;
  		  macs:units = "m^2/kg" ;
  	double eqf(distribution, lambda) ;
  		  eqf:long_name = "Extinction quality factor" ;
  		  eqf:units = "-" ;
  	double sqf(distribution, lambda) ;
  		  sqf:long_name = "Scattering quality factor" ;
  		  sqf:units = "-" ;
  	double aqf(distribution, lambda) ;
  		  aqf:long_name = "Absorption quality factor" ;
  		  aqf:units = "-" ;
  	double g(distribution, lambda) ;
  		  g:long_name = "Asymmetry parameter" ;
  		  g:units = "-" ;
  	double ssa(distribution, lambda) ;
  		  ssa:long_name = "Single scattering albedo" ;
  		  ssa:units = "-" ;
  	double phase_func(distribution, lambda, th) ;
  		  phase_func:long_name = "Mie scattering phase function as a function of theta" ;
  		  phase_func:units = "m^-1.sr^-1" ;
  	double pdf(distribution, lambda, th) ;
  		  pdf:long_name = "Mie scattering angle density probability as a function of theta" ;
  		  pdf:units = "-" ;
  	double cpdf(distribution, lambda, th) ;
  		  cpdf:long_name = "Cumulative Mie scattering angle density probability" ;
  		  cpdf:units = "-" ;
  	double icp(distribution, lambda, cp) ;
  		  icp:long_name = "Inverse of cumulative Mie scattering angle density probability" ;
  		  icp:units = "radian" ;
  	double mu_icp(distribution, lambda, cp) ;
  		  mu_icp:long_name = "Cosine of inverse of cumulative Mie scattering angle density probability" ;
  		  mu_icp:units = "-" ;
  	char name(distribution) ;
  		  name:long_name = "Name or identifier for the droplet size distribution" ;
  		  name:units = "-" ;
  	double reff(distribution) ;
  		  reff:long_name = "Effective radius of droplet size distribution" ;
  		  reff:units = "kilometers" ;
  	double veff(distribution) ;
  		  veff:long_name = "Effective variance of droplet size distribution" ;
  		  veff:units = "micrometers" ;
}
\end{verbatim}

Les fichiers netCDF sont dit auto-documentés. On accède au header par la commande 
\begin{verbatim}
ncdump -h fichier.nc
\end{verbatim}
ou bien 
\begin{verbatim}
less fichier.nc
\end{verbatim}
Les outils netcdf permettent d'explorer et de manipuler ces fichiers, par exemple 
\begin{verbatim}
ncks -d lambda,0,4 -v g,mecs,ssa fichier.nc 
\end{verbatim}
va afficher les valeurs des variables g, mecs et ssa pour les cinq premières longueurs d'ondes (lambda indicé de 0 à 4) :
\begin{verbatim}
netcdf Mie_LUT_Cloud-2-7-0.1 {
  dimensions:
    distribution = UNLIMITED ; // (1 currently)
    lambda = 5 ;

  variables:
    double g(distribution,lambda) ;
      g:long_name = "Asymmetry parameter" ;
      g:units = "-" ;

    double lambda(lambda) ;
      lambda:long_name = "Wavelength" ;
      lambda:units = "nanometers" ;

    double mecs(distribution,lambda) ;
      mecs:long_name = "Massic extinction cross section" ;
      mecs:units = "m^2/kg" ;

    double ssa(distribution,lambda) ;
      ssa:long_name = "Single scattering albedo" ;
      ssa:units = "-" ;

  data:
    g = 
    0.85158870117106, 0.85791211408128, 0.86041127930509, 0.86188076421588, 0.8624284438282 ;

    lambda = 200, 225, 250, 275, 300 ;

    mecs = 
    220.848197337089, 221.356833683458, 221.840849820344, 222.330344372405, 222.791962061614 ;

    ssa = 
    0.99995336090809, 0.99998154106509, 0.99998904440106, 0.99999285801956, 0.99999558891286 ;

} // group /
\end{verbatim}

On peut également concaténer les tables selon la dimension UNLIMITED "distribution" 
\begin{verbatim}
ncrcat fichier1.nc fichier2.nc fichier3.nc fichier_concatene.nc
\end{verbatim}
on obtient alors un unique fichier dont la dimension UNLIMITED "distribution" vaut "3 currently". Une fois concaténés, on peut réaliser des opérations sur les variables, par exemple moyenner les variables selon la dimension distribution : 
\begin{verbatim}
ncra -O -y avg fichier_concatene.nc fichier_moyenne.nc
\end{verbatim}

\bibliographystyle{unsrt}
\bibliography{ref}

\end{document}
