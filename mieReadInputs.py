import numpy as np
#               CHARGEMENT NOM FICHIERS                   #
def loadFileNames(nomFichiers) :
  fileNames={}
  f=open(nomFichiers,"r")
  for line in f :
    if ":" in line :
      name=line.split(":")[-1].strip()
    else : continue
    if "ongueurs d'onde" in line :
      fileNames["lambdaFile"] = name+".txt"
    elif "ndices complexe" in line :
      fileNames["indiceFile"] = name+".txt"
    elif "istribution de " in line :
      fileNames["geometrieFile"] = name+".txt"
    elif "omposition inte" in line :
      fileNames["compoFile"] = name+".txt"
    elif "pectre sections (m2)" in line :
      fileNames["sectionsPartFile"] = name+".txt"
    elif "pectre sections (m2/kgx)" in line :
      fileNames["sectionsMassFile"] = name+".txt"
    elif "pectre efficacites" in line :
      fileNames["efficaciteFile"] = name+".txt"
    elif "pectre parametre asymetrie" in line :
      fileNames["gFile"] = name+".txt"
    elif "pectre albedo" in line :
      fileNames["albedoFile"] = name+".txt"
    elif  "pectre cumulee inverse" in line :
      fileNames["cumuleeInvFile"] = name+".txt"
    elif "onctions de phase" in line : 
      fileNames["pfFile"] = name 
    elif "Readme cumulee inverse" in line :
      fileNames["cumuleeInvReadmeFile"] =name+".txt"
    elif "Maillage lambda cumulee" in line :
      fileNames["cumuleeInvLambdaFile"] =name+".txt"
    elif "Maillage Cdf cumulee" in line : 
      fileNames["cumuleeInvCdfFile"] =name+".txt"
    else : continue
  f.close()
  return fileNames

#          CHARGEMENT PARAMETRES GEOMETRIQUES             #
def loadGeometrie(geometrieFile) :
  geomParams={}
  f=open(geometrieFile,"r")
  for line in f :
    if ':' in line :
        name=line.split(":")[-1].strip()
    else : continue
    if "Distribution shape" in line :
      geomParams["distShape"] = np.int(name)
    if "Rayon effectif (microns)" in line :
      geomParams["rEffectif"] = np.float(name)
    if "Variance effective (microns)" in line :
      geomParams["vEffectif"] = np.float(name)
  f.close();
  return geomParams


#               CHARGEMENT COMPOSITION                    #
def loadComposition(compoFile) :
  f=open(compoFile,"r")
  for line in f : 
    if ':' in line :
        name=line.split(":")[-1].strip()
    else : continue
    if "Masse volumique du catalyseur" in line :
      rhoSec = np.float(name)
  f.close()
  return rhoSec

#              CHARGEMENT SPECTRE INDICE                  #
def loadIndice(indiceFile) :
  indicesRef = {}
  indicesRef["indiceLambda"] = [] 
  indicesRef["indiceRe"    ] = [] 
  indicesRef["indiceIm"    ] = [] 
  indicesRef["indiceMilieu"] = [] 
  f=open(indiceFile,"r")
  for line in f : 
    if line.startswith("#") : continue 
    else :
      i1,i2,i3,i4 = [np.float(u) for u in line.split("\t")]
      indicesRef["indiceLambda"]+=[i1]
      indicesRef["indiceRe"    ]+=[i2]
      indicesRef["indiceIm"    ]+=[i3]
      indicesRef["indiceMilieu"]+=[i4]
  f.close()
  return indicesRef

