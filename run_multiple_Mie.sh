
# Run Mie.py for various geometries (size distributions)
p="."
base=$p/Entrees/geometrie-base.txt

for nshape in 2 4 
do
  for reff in 7 10 13 16 
  do
    for seff in 0.01 0.1 0.175 0.25 # already computed
    do
      # Où en est-on ? 
      echo $nshape $reff $seff
  
      # Prepare entrees 
      new=$p/Entrees/geometrie-${nshape}_${reff}_${seff}.txt
      cp $base $new
      sed -i "s/_ND_/"$nshape"/g" $new
      sed -i "s/_Re_/"$reff"/g" $new
      sed -i "s/_Ne_/"$seff"/g" $new
      cp $new $p/Entrees/geometrie.txt

      # Run Mie.pl
      python Mie.py > log-Miepy-${nshape}_${reff}_$seff.out 2>&1

      # Prepare sorties
      rm -f $p/Sorties/ALL_PF.txt
      cat $p/Sorties/PF*txt >> $p/Sorties/ALL_PF.txt
      eval $p/mie_write2ncdf

      # Save sorties and LUT
      zip $p/sorties_${nshape}_${reff}_${seff}.zip $p/Sorties/*
      mv $p/Mie_LUT_Cloud.nc $p/Mie_LUT_Cloud-${nshape}-${reff}-${seff}.nc
    done
  done
done
