# coding: utf-8
import numpy as np 
from scipy.integrate import quad
from scipy.special import gamma as Gamma

rhow_kgm3 = 1e3 # kg/m3
rhow_kgum3 = 1e-15 # kg/um3
# Paramètres des distributions 
def alpha(reff,seff) : 
    return (1. - 3.*seff)/seff

def rmod(reff,seff)  : 
    return (1. - 3.*seff)*reff

def logn_reff(rmoy,sigm)  : 
    return rmoy*np.exp(5./2.*np.log(sigm)**2) 

def r1(rbarre,s) :
    return rbarre*np.exp(1/2.*np.log(s)**2)

def rnpdf(r,pdf,n,a1,a2) : # n-th moment of pdf 
    return r**n*pdf(r,a1,a2)

def rreffpdf(r,pdf,a1,a2,reff) : # pdf(r)(r-reff)r^2
    return pdf(r,a1,a2)*(r-reff)*r**2

def mrpdf(r,pdf,a1,a2) :  # mass density for radius in r,r+dr
    return rhow_kgum3*4./3.*np.pi*r**3*pdf(r,a1,a2)

def moment(pdf,n,a1,a2) :
    mom = quad(rnpdf,0.0001,1000,args=(pdf,n,a1,a2))[0]
    return mom

def compute_reff(pdf,a1,a2):
    m3 = moment(pdf,3,a1,a2)
    m2 = moment(pdf,2,a1,a2)
    if m2 == 0 : return 0
    else : return m3/m2

def compute_veff(pdf,a1,a2):
    reff=compute_reff(pdf,a1,a2)
    veff=quad(rreffpdf,0.0001,1000,args=(pdf,a1,a2,reff))[0]
    r2 = moment(pdf,2,a1,a2)
    return veff/(r2*reff**2)

def compute_Me(pdf,r1,r2,a1,a2) : 
    return quad(mrpdf,r1,r2,args=(pdf,a1,a2))[0]

def find_rmin(pdf, a1, a2, eps) :
    rmin = 0 ; rmax = 50
    r = (rmax+rmin)/2.
    cdf = quad(pdf,0,r,args=(a1,a2))[0]
    while abs(cdf-eps)>1e-8 :
        if cdf > eps :
            rmax = r*1. ;
            r = (rmax+rmin)/2. 
        else : 
            rmin = r*1. ;
            r = (rmax+rmin)/2. 
        cdf = quad(pdf,0,r,args=(a1,a2))[0]
    print("neglect",cdf, "before rmin")
    return r

def find_rmax(pdf, a1, a2, eps) :
    rmin = 0 ; rmax = 100
    r = (rmax+rmin)/2.
    cdf = quad(pdf,r,1000,args=(a1,a2))[0]
    i=0
    while abs(cdf-eps)>1e-8 :
        if cdf < eps :
            rmax = r*1. ;
            r = (rmax+rmin)/2. 
        elif cdf > eps :
            rmin = r*1. ;
            r = (rmax+rmin)/2. 
        cdf = quad(pdf,r,1000,args=(a1,a2))[0]
    print("neglect",cdf, "after rmax")
    return r

# Normalisation de la fonction gamma
def No(reff,seff) :
    return 1./((reff*seff)**(1./seff-2))/Gamma(1./seff-2)

# Distribution gamma et log-normale
def gamma(r,alpha,rmod) : # non normalisée 
    return r**(alpha)*np.exp(-alpha*r/rmod)
def pgamma(r,reff,seff) : # normalisée 
    return gamma(r,alpha(reff,seff),rmod(reff,seff))*No(reff,seff)

def lognorm(r,rmoy,s) : # normalisée 
    # rmoy = rbarre dans distrib_lognormale_VincentEymet
    return np.exp(-(np.log(r)-np.log(rmoy))**2./(2.*(np.log(s))**2.))/(np.sqrt(2.*np.pi)*r*np.log(s))

def plognorm(r,reff,seff) :
    # ln2sg = log(seff + 1)
    # rmoy = re exp (-5/2 ln2sg)
    ln2sg = np.log(seff+1)
    rmoy  = reff*np.exp(-5./2.*ln2sg)
    s     = np.exp(np.sqrt(ln2sg))
    return lognorm(r,rmoy,s)

if __name__=="__main__":
  from mycolors import rainbow4 as cols
  import matplotlib.pyplot as plt 

  rmin = 0.001
  rmax = 1000
  rVec = np.linspace(rmin,rmax/50,1000)
  
  # Parameters rbarre et s dans 
  # distrib_lognormale_VincentEymet.pdf
  rbarre=9.76      # rayon moyen
  s=1.1052         # ~ largeur distrib
  # Dans distrib_lognormale_VincentEymet :
  # mu = np.log(rbarre)
  # sigma = np.log(s)
  
  # print(rbarre, r1(rbarre,s), reff(rbarre,s))
  # print("lognormal with parameters rbarre = %g, s = %g"%(rbarre,s))
  # print("integrates to %g from %g to %g"%(quad(lognorm,rmin,rmax,args=(rbarre,s))[0],rmin,rmax))
  reff=logn_reff(rbarre,s)
  for iseff,seff in enumerate([0.01,0.1,0.175,0.25]) :
    print("seff = %g ; s = %g"%(seff, np.exp(seff)))
    rMin = find_rmin(pgamma,reff,seff,0.0001)
    rMax = find_rmax(pgamma,reff,seff,0.0001)
    #rTemp = 1.e-10
    #while (pgamma(rTemp,reff,seff)<1.e-4) :
    #  rTemp += 0.01
    #rMin = rTemp
    #rTemp += 0.05
    #while (pgamma(rTemp,reff,seff)>1.e-4) :
    #  rTemp += 0.01
    #rTemp += 0.05
    #rMax = rTemp
    plt.plot(rVec, pgamma(rVec,reff,seff), label="Gamma-Ne-%g"%(seff))#,color=cols[iseff+1])
    # print("gamma with parameters Re = %g, s = %g" %(reff,seff))
    print("integrates to %g from %g to %g"%(quad(pgamma,rMin,rMax,args=(reff,seff))[0],rMin,rMax))
    print("Effective mass %g" %compute_Me(pgamma,rMin,rMax,reff,seff))
    #print("computed reff gamma : %g "%(compute_reff(pgamma,reff,seff)))
    #print("computed reff lognorm %g or %g "%(compute_reff(lognorm,rbarre,np.exp(seff)), logn_reff(rbarre,np.exp(seff))))
  
  #plt.plot(rVec, lognorm(rVec,rbarre,np.exp(seff)), label="lognm Ne %g"%(seff),ls="--",color=cols[iseff+1])
  plt.plot(rVec,lognorm(rVec,rbarre,s),"k--", label="Lognormal")
  plt.legend(loc="best")
  plt.ylim(0,0.5)
  plt.xlabel("Radius [$\mu$m]")
  plt.ylabel("Density number of particles with radius r")
  plt.title("Gamma size distributions")
  plt.savefig("distribution_gamma_lognormal.pdf")
  #plt.show()
